;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-ignore-case nil)
 '(column-number-mode t)
 '(custom-safe-themes
   (quote
	("d1ede12c09296a84d007ef121cd72061c2c6722fcb02cb50a77d9eae4138a3ff" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "bd7b7c5df1174796deefce5debc2d976b264585d51852c962362be83932873d9" default)))
 '(fill-column 80)
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
	(solarized-theme yaml-mode flymake-yaml flymake-python-pyflakes doremi-cmd color-theme-solarized w3m tabbar python-pep8 py-autopep8 popup-complete pep8 nav markdown-mode jedi helm color-theme)))
 '(sentence-end-double-space nil)
 '(show-paren-mode t)
 '(show-trailing-whitespace t)
 '(speedbar-show-unknown-files t)
 '(speedbar-update-flag nil)
 '(sr-speedbar-auto-refresh nil)
 '(sr-speedbar-default-width 30)
 '(sr-speedbar-max-width 30)
 '(sr-speedbar-right-side nil)
 '(tool-bar-mode nil)
 '(typescript-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; packages to install
(setq package-list '(auto-complete fill-column-indicator flx-ido flycheck
								   flycheck-pyflakes git-commit monokai-theme
								   org php-mode sr-speedbar projectile vue-mode
								   web-mode
								   ))

;; package repos
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
			("melpa" . "http://melpa.milkbox.net/packages/")
			("elpa" . "http://tromey.com/elpa/")
			))

;; activate all the packages
(package-initialize)

;; Ensure we have downloaded archive description or use package-archive-contents
(or (file-exists-p package-user-dir)
    (package-refresh-contents))

;; install the missing packages
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))


;; PHP mode
(autoload 'php-mode "php-mode" "Major mode for editing php code." t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))

;; Vue mode
(add-to-list 'auto-mode-alist '("\\.vue$" . vue-mode))

;; Disable background-color (for vue-mode)
(add-hook 'mmm-mode-hook
          (lambda ()
            (set-face-background 'mmm-default-submode-face nil)))

;; Load python-mode for scons files
(setq auto-mode-alist(cons '("SConstruct" . python-mode) auto-mode-alist))
(setq auto-mode-alist(cons '("SConscript" . python-mode) auto-mode-alist))

;; Set default font
(set-face-attribute 'default nil
                    :family "Source Code Pro"
                    :height 120
                    :weight 'normal
                    :width 'normal)

;; Window size
(defun set-frame-size-according-to-resolution ()
  (interactive)
  (if window-system
  (progn
    ;; use 120 char wide window for largeish displays
    ;; and smaller 80 column windows for smaller displays
    ;; pick whatever numbers make sense for you
    (if (> (x-display-pixel-width) 1280)
	   (add-to-list 'default-frame-alist (cons 'width 120))
	   (add-to-list 'default-frame-alist (cons 'width 80)))
    ;; for the height, subtract a couple hundred pixels
    ;; from the screen height (for panels, menubars and
    ;; whatnot), then divide by the height of a char to
    ;; get the height we want
    (add-to-list 'default-frame-alist
	 (cons 'height (/ (- (x-display-pixel-height) 200)
			     (frame-char-height)))))))

(set-frame-size-according-to-resolution)

;; Shrink window horizontally 10
(global-set-key (kbd "C-c v") (kbd "C-u 10 C-x {"))

;; default tab width 4 spaces
(setq-default tab-width 4)

;; w3m 80 col
(setq w3m-fill-column 80)

;; In Windows, use 'plink' for TRAMP
(when (eq window-system 'w32)
  (setq tramp-default-method "plink"))

(add-hook 'after-init-hook 'my-after-init-hook)
(defun my-after-init-hook ()
  ;; do things after package initialization

  ;; Auto Complete
  (require 'auto-complete-config)
  (ac-config-default)
  (global-auto-complete-mode t)
  ;;(add-hook 'after-change-major-mode-hook 'auto-complete-mode)

  ;; JS Code Folding
  (add-hook 'js-mode-hook
	    (lambda ()
	      ;; Scan the file for nested code blocks
	      (imenu-add-menubar-index)
	      ;; Activate the folding mode
	      (hs-minor-mode t)))
  ;; Show-hide
  (global-set-key (kbd "") 'hs-show-block)
  (global-set-key (kbd "") 'hs-show-all)
  (global-set-key (kbd "") 'hs-hide-block)
  (global-set-key (kbd "") 'hs-hide-all)

  ;; pyflakes flymake
  (when (load "flymake" t)
    (defun flymake-pyflakes-init ()
      (let* ((temp-file (flymake-init-create-temp-buffer-copy
			 'flymake-create-temp-inplace))
	     (local-file (file-relative-name
			  temp-file
			  (file-name-directory buffer-file-name))))
	(list "pyflakes" (list local-file))))
    (add-to-list 'flymake-allowed-file-name-masks
				 '("\\.py\\'" flymake-pyflakes-init))
	;; These can be pretty annoying when using globals SCons provides between
	;; between files, but useful when checking for bad imports, etc.
	;;(add-to-list 'flymake-allowed-file-name-masks
	;;			 '("\\SConstruct\\'" flymake-pyflakes-init))
	;;(add-to-list 'flymake-allowed-file-name-masks
	;;			 '("\\SConscript\\'" flymake-pyflakes-init))
	;;(add-to-list 'flymake-allowed-file-name-masks
	;;			 '("\\SConscript-env\\'" flymake-pyflakes-init))
	)

  (add-hook 'find-file-hook 'flymake-find-file-hook)

  ;; disable flymake on HTML
  (delete '("\\.html?\\'" flymake-xml-init) flymake-allowed-file-name-masks)

  ;; Fill Column Indicator
  (require 'fill-column-indicator)
  (add-hook 'after-change-major-mode-hook 'fci-mode)

  ;; 80 col fill
  (setq-default fill-column 80)

  ;; Python mode defaults
  (setq interpreter-mode-alist
	(cons '("python" . python-mode)
	      interpreter-mode-alist)
	python-mode-hook
	'(lambda () (progn
		      (set-variable 'show-trailing-whitespace t)
		      (set-variable 'fill-column 79)
		      (set-variable 'py-indent-offset 4)
		      (set-variable 'indent-tabs-mode nil))))

  ;; Show trailing whitespace and tabs in Python
  (require 'whitespace)
  (setq whitespace-display-mappings
	;; all numbers are Unicode codepoint in decimal. try (insert-char 182 ) to see it
	'(
	  (space-mark 32 [183] [46]) ; 32 SPACE, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
	  (newline-mark 10 [182 10]) ; 10 LINE FEED
	  (tab-mark 9 [187 9] [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE 「▷」
	  ))
  (setq whitespace-style '(face tabs trailing tab-mark))
  (set-face-attribute 'whitespace-tab nil
		      :background "#f0f0f0"
		      :foreground "#00a8a8"
		      :weight 'bold)
  (set-face-attribute 'whitespace-trailing nil
		      :background "#e4eeff"
		      :foreground "#183bc8"
		      :weight 'normal)
  (add-hook 'python-mode-hook 'whitespace-mode)

  ;; Automatically remove trailing whitespace when Python file is saved
  (add-hook 'python-mode-hook
	    (lambda()
	      (add-hook 'local-write-file-hooks
			'(lambda()
			   (save-excursion
			     (delete-trailing-whitespace))))))

  ;; use web-mode for django templates
  ;; tab levels are 2 spaces
  (require 'web-mode)
  (add-to-list 'auto-mode-alist '("/\\templates\\/.*\\.html\\'" . web-mode))
  (setq web-mode-engines-alist '(("django" . "/\\templates\\/.*\\.html\\'")))
  (add-hook 'web-mode-hook
			(lambda ()
			  (setq indent-tabs-mode nil)
			  (setq web-mode-markup-indent-offset 2)
			  (setq tab-width 2)))

  ;; 2 space indent for JS
  (add-hook 'js-mode-hook
			(lambda ()
			  (setq indent-tabs-mode nil)
			  (setq js-indent-level 2)
			  (setq tab-width 2)))

  (load-theme 'monokai)

  (require 'projectile)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1)

  ;; load local init
  (load "~/.emacs.local")
)
